<?php
set_time_limit(0);
ignore_user_abort(true);

$opt = array(
    "type:",
    "appName:",
    "webUrl:",
    "topMessage:",
    "bottomMessage:",
    "hasSplash:",
    "splashDuration:",
    "appIcon:",
    "splashIcon:"
);

// Get field data provided from cli by user.
$options = getopt("meirda", $opt);

// Check all fields are provided or not.
checkRequiredMasala($opt, $options);

// Do some magic according to type.
if ($options["type"] === "A") {

    doSomeChamatkar(prepareApkFields($options), "http://159.203.171.53/javatutorial/apktool/JavaController.php", "Android");

} else if ($options["type"] === "I") {

    doSomeChamatkar(prepareIpaFields($options), "http://159.203.171.53/ipa/services/ipa.php", "IOS");

} else if ($options["type"] === "B") {

    doMahaChamatkar(prepareApkFields($options), prepareIpaFields($options));

} else {

    die("Please provide correct type.");

}


// Check whether the user has provided all fields.
function checkRequiredMasala($opt, $options)
{
    $error = " ";
    for ($i = 0; $i < sizeof($opt); $i++) {
        if (!isset($options[str_replace(":", "", $opt[$i])])) {

            $error = $error . str_replace(":", "", $opt[$i]) . " ";
            if ($i == (sizeof($opt) - 1)) {
                die("\nPlease provide " . $error . " params.\n");
            }

        }
    }
    $a = explode('.', $options['appIcon']);
    $b = explode('.', $options['splashIcon']);
    $appIcon_ext = strtolower(end($a));
    $splashIcon_ext = strtolower(end($b));

    if ($appIcon_ext !== 'png' || $splashIcon_ext !== 'png') {
        die("\nPlease provide appIcon or splashIcon with png image only\n");
    }

}


// Make request either for apk or ipa.
function doSomeChamatkar($params, $url, $header)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $response = curl_exec($ch);
    $response = json_decode($response);
    giveBlackMagicResults($response, $header);
}


// Make requests for both apk and ipa.
function doMahaChamatkar($apkFields, $ipaFields)
{
    $nodes = array("http://159.203.171.53/javatutorial/apktool/JavaController.php", "http://159.203.171.53/ipa/services/ipa.php");
    $header = array("Android", "IOS");
    $node_count = count($nodes);

    $curl_arr = array();
    $master = curl_multi_init();

    for ($i = 0; $i < $node_count; $i++) {
        $url = $nodes[$i];
        $curl_arr[$i] = curl_init($url);
        curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_arr[$i], CURLOPT_POST, 1);
        curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_arr[$i], CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data'));
        if ($i == 0) {
            curl_setopt($curl_arr[$i], CURLOPT_POSTFIELDS, $apkFields);
        } else {
            curl_setopt($curl_arr[$i], CURLOPT_POSTFIELDS, $ipaFields);
        }
        curl_multi_add_handle($master, $curl_arr[$i]);
    }

    do {
        curl_multi_exec($master, $running);
    } while ($running > 0);


    for ($i = 0; $i < $node_count; $i++) {
        $results[] = curl_multi_getcontent($curl_arr[$i]);
        $response = json_decode($results[$i]);
        giveBlackMagicResults($response, $header[$i]);
    }

}


//prepare field for apk url.
function prepareSomeChatni($options)
{
    return "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" .
        "<resources>" .
        "<string name=\"abc_action_bar_home_description\">Navigate home</string>" .
        "<string name=\"abc_action_bar_home_description_format\">%1\$s, %2\$s</string>" .
        "<string name=\"abc_action_bar_home_subtitle_description_format\">%1\$s, %2\$s, %3\$s</string>" .
        "<string name=\"abc_action_bar_up_description\">Navigate up</string>" .
        "<string name=\"abc_action_menu_overflow_description\">More options</string>" .
        "<string name=\"abc_action_mode_done\">Done</string>" .
        "<string name=\"abc_activity_chooser_view_see_all\">See all</string>" .
        "<string name=\"abc_activitychooserview_choose_application\">Choose an app</string>" .
        "<string name=\"abc_capital_off\">OFF</string>" .
        "<string name=\"abc_capital_on\">ON</string>" .
        "<string name=\"abc_font_family_body_1_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_body_2_material\">sans-serif-medium</string>" .
        "<string name=\"abc_font_family_button_material\">sans-serif-medium</string>" .
        "<string name=\"abc_font_family_caption_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_display_1_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_display_2_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_display_3_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_display_4_material\">sans-serif-light</string>" .
        "<string name=\"abc_font_family_headline_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_menu_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_subhead_material\">sans-serif</string>" .
        "<string name=\"abc_font_family_title_material\">sans-serif-medium</string>" .
        "<string name=\"abc_search_hint\">Searchâ€¦</string>" .
        "<string name=\"abc_searchview_description_clear\">Clear query</string>" .
        "<string name=\"abc_searchview_description_query\">Search query</string>" .
        "<string name=\"abc_searchview_description_search\">Search</string>" .
        "<string name=\"abc_searchview_description_submit\">Submit query</string>" .
        "<string name=\"abc_searchview_description_voice\">Voice search</string>" .
        "<string name=\"abc_shareactionprovider_share_with\">Share with</string>" .
        "<string name=\"abc_shareactionprovider_share_with_application\">Share with %s</string>" .
        "<string name=\"abc_toolbar_collapse_description\">Collapse</string>" .
        "<string name=\"app_name\">" . strval($options['appName']) . "</string>" .
        "<string name=\"has_splash\">" . strval($options['hasSplash']) . "</string>" .
        "<string name=\"search_menu_title\">Search</string>" .
        "<string name=\"splash_duration\">" . strval($options['splashDuration']) . "</string>" .
        "<string name=\"splash_message_one\">" . strval($options['topMessage']) . "</string>" .
        "<string name=\"splash_message_two\">" . strval($options['bottomMessage']) ."000" . "</string>" .
        "<string name=\"status_bar_notification_info_overflow\">999.</string>" .
        "<string name=\"webview_url\">" . strval($options['webUrl']) . "</string>" .
        "</resources>";
}

// prepare data to send to apk url.
function prepareApkFields($options)
{
    if (function_exists('curl_file_create')) { // php 5.5+
        $appiconpath = curl_file_create(realpath($options['appIcon']));
        $splashiconpath = curl_file_create(realpath($options['splashIcon']));

    } else { //
        $appiconpath = '@' . realpath($options['appIcon']);
        $splashiconpath = '@' . realpath($options['splashIcon']);
    }
    return array('type' => 'executeApkProgram', 'stringresources' => prepareSomeChatni($options), 'appicon' => $appiconpath, 'companylogo' => $splashiconpath);
}

// prepare data to send to ipa url.
function prepareIpaFields($options)
{
    if (function_exists('curl_file_create')) { // php 5.5+
        $appiconpath = curl_file_create(realpath($options['appIcon']));
        $splashiconpath = curl_file_create(realpath($options['splashIcon']));

    } else { //
        $appiconpath = '@' . realpath($options['appIcon']);
        $splashiconpath = '@' . realpath($options['splashIcon']);
    }
    return array('type' => 'saveIPA', 'appName' => $options['appName'], 'webUrl' => $options['webUrl'], 'hasSplash' => $options['hasSplash'], 'splashDuration' => $options['splashDuration'], 'topMessage' => $options['topMessage'], 'bottomMessage' => $options['bottomMessage'], 'logoUrl' => $appiconpath, 'SplashlogoUrl' => $splashiconpath);
}

// provide url of generated apk and ipa.
function giveBlackMagicResults($response, $header)
{
    if ($response->Status === 'Success') {
        echo "\n$header :-->\n\n" . $response->url . "\n";
    } else {
        echo "\n" . $response->Message . "\n";
    }
}

?>